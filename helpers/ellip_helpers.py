import os
import time
import jenkins
import getpass
from nbconvert.preprocessors import ExecutePreprocessor, CellExecutionError
import nbformat as nbf
import lxml.etree as etree
import owslib
from owslib.wps import monitorExecution
from owslib.wps import WebProcessingService
from nbconvert.preprocessors import ExecutePreprocessor, CellExecutionError

class EllipBuild():

    def __init__(self, configuration_notebook, build_type, jenkins_username, jenkins_api_key):
        

        nb_config = configuration_notebook

        nb = nbf.read(nb_config, 4)

        exec(nb['cells'][2]['source']) in globals(), locals()
        
        if build_type == 'App':
            
            self.app = dict([('artifact_id', app_artifact_id),
                             ('folder', folder),
                             ('repository', repository),
                             ('community', community)]) 

            self.job = 'communities/%s/%s/applications/%s/docker' % (self.app['repository'],
                                                                        self.app['folder'], 
                                                                        self.app['artifact_id'])
            
        elif build_type == 'Trigger':
            
            self.app = dict([('artifact_id', trigger_queue_artifact_id),
                             ('repository', repository),
                             ('folder', folder),
                             ('community', community)])
            
            self.job = 'communities/%s/%s/triggers/%s/docker' % (self.app['repository'],
                                                                    self.app['folder'],
                                                                    self.app['artifact_id'])
        
        self.server = jenkins.Jenkins(url=ci_endpoint,
                                      username=jenkins_username,
                                      password=jenkins_api_key)
        
        
        self.sleep_secs = 3
        self.build_number = None
    
    def submit(self):
    
        last_build = self.server.get_job_info(self.job)['lastCompletedBuild']['number']
    
        self.server.build_job(self.job) 
    
        while self.server.get_job_info(self.job)['lastBuild']['number'] == last_build:
        
           time.sleep(self.sleep_secs)
        
        self.build_number = self.server.get_job_info(self.job)['lastBuild']['number']
        
        return self.build_number
    
    def monitor(self):
 
        if not self.build_number:
        
            raise IndexError

        while self.server.get_build_info(self.job, self.build_number)['building']:
        
            time.sleep(self.sleep_secs)
    
        if str(self.server.get_build_info(self.job, self.build_number)['result']) == 'SUCCESS':
        
            return True
    
        if str(self.server.get_build_info(self.job, self.build_number)['result']) == 'FAILURE':
        
            return False
        
class EllipDeploy():
    
    def __init__(self, configuration_notebook, deployment_type, api_key):

        nb_config = configuration_notebook

        nb = nbf.read(nb_config, 4)

        exec(nb['cells'][2]['source']) in globals(), locals()
        exec(nb['cells'][4]['source']) in globals(), locals()
        
        self.api_key = api_key

        if deployment_type == 'App':
        
            self.app = dict([('artifact_id', app_artifact_id),
                             ('version', app_version),
                             ('repository', repository),
                             ('community', community)])
            
            self.wps_url = '%s/zoo-bin/zoo_loader.cgi' % production_centre_apps_deployer_endpoint
            
        elif deployment_type == 'Trigger':
            
            self.app = dict([('artifact_id', trigger_queue_artifact_id),
                             ('version', trigger_queue_version),
                             ('repository', repository),
                             ('folder', folder),
                             ('community', community)])
            
            self.wps_url = '%s/zoo-bin/zoo_loader.cgi' % trigger_deployer
        
        else:
            
            raise Exception('Deployment type not supported')
        
        
        self.app_process_id = '%s_%s_%s_%s' % (self.app['community'].replace('-', '_'), self.app['artifact_id'].replace('-', '_'), self.app['artifact_id'].replace('-', '_'), self.app['version'].replace('.', '_'))  
        
        
        self.ows_context_url = '/%s/_applications/%s/%s/%s/%s-%s-application-context.xml' % (self.app['community'], 
                                                                                self.app['community'], 
                                                                                self.app['artifact_id'],
                                                                                self.app['version'], 
                                                                                self.app['artifact_id'],
                                                                                self.app['version'])
      

    def deploy(self):
        
        self.wps = WebProcessingService(self.wps_url,
                                        verbose=False, 
                                        skip_caps=False)
      
        deploy_process_available = False

        process_id = 'TerradueDeployProcess'
        
        for index, elem in enumerate(self.wps.processes):

            if process_id in elem.identifier:

                deploy_process_available = True

        if deploy_process_available:
            
            print '%s is available' % process_id
            
        else:
            
            raise Exception('%s is not available' % process_id)
        
        process = self.wps.describeprocess(process_id)
        
        inputs = [('applicationPackage', self.ows_context_url),
                  ('apikey', self.api_key)]
        
        execution = owslib.wps.WPSExecution(url=self.wps.url)

        execution_request = execution.buildRequest(process_id,
                                                   inputs, 
                                                   output=[('deployResult', False)])
        
        execution_response = execution.submitRequest(etree.tostring(execution_request))
        
        execution.parseResponse(execution_response)
        
        monitorExecution(execution)
        
        if execution.isSucceded(): 
            
            print '%s deployment is successful' % self.app_process_id

        else: 

            raise Exception('%s deployment failed' % self.app_process_id)
            
    def check_deployment(self):
        
        wps = WebProcessingService(self.wps_url,
                                   verbose=False, 
                                   skip_caps=False)

        wps.getcapabilities()

        app_deployed = False

        for index, elem in enumerate(wps.processes):
            if elem.identifier == self.app_process_id:
                
                app_deployed = True

        if app_deployed:
            print 'Process %s deployed' % self.app_process_id
        else:
            raise Exception('Process %s not deployed' % self.app_process_id)